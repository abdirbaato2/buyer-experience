---
  secure:
    content: Secure
    copy_media:
      header: Secure
      aos_animation: fade-up
      aos_duration: 500
      icon_url: /nuxt-images/icons/devops/secure-colour.svg
      subtitle: Security capabilities, integrated into your development lifecycle.
      text: GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications along with license compliance.
      image:
        url: /nuxt-images/devops-graphics/secure.png
        alt: Secure
      categories:
        - Static Application Security Testing
        - Secret Detection
        - Code Quality
        - Dynamic Application Security Testing
        - API Security
        - Fuzz Testing
        - Dependency Scanning
        - License Compliance
        - Vulnerability Database
        - Vulnerability Management
  feature_content:
    products_category:
      products:
        # Static Application Security Testing
        - title: Static Application Security Testing
          categories:
            - Static Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab allows easily running Static Application Security Testing (SAST) in CI/CD pipelines; checking for vulnerable source code or well known security bugs in the libraries that are included by the application. Results are then shown in the Merge Request and in the Pipeline view. This feature is available as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-sast) to provide security-by-default.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/sast/
            text: Documentation
            data_ga_name: Static Application Security Testing
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Configuration UI
          categories:
            - Static Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Enabling SAST is now as simple as two clicks. This guided configuration experience makes it easier for non-CI experts to get started with GitLab SAST. The tool helps a user create a merge request to enable SAST scanning while leveraging best configuration practices like using the GitLab-managed [`SAST.gitlab-ci.yml` template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml) and properly [overriding template settings](https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings).
          link:
            href: https://docs.gitlab.com/ee/user/application_security/configuration/
            text: Documentation
            data_ga_name: Configuration UI
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Custom Rulesets for SAST
          categories:
            - Static Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab SAST allows users to change the vulnerability detection defaults to tailor results to their organization's preferences. SAST custom rulesets allow you to exclude rules and modify the behavior of existing rules.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/sast/#custom-rulesets
            text: Documentation
            data_ga_name: Custom Rulesets for SAST
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Infrastructure as Code (IaC) Security Scanning
          categories:
            - Static Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: With Gitlab 14.5 we're introducing security scanning for Infrastructure as Code (IaC) configuration files.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/iac_scanning/
            text: Documentation
            data_ga_name: Infrastructure as Code (IaC) Security Scanning
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Secret Detection
        - title: Secret Detection
          categories:
            - Secret Detection
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab allows you to perform Secret Detection in CI/CD pipelines; checking for unintentionally committed secrets and credentials. Results are then shown in the Merge Request and in the Pipeline view. This feature is available as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) to provide security-by-default.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/secret_detection/
            text: Documentation
            data_ga_name: Secret Detection
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Custom Rulesets for Secret Detection
          categories:
            - Secret Detection
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab Secret Detection allows users to change the vulnerability detection defaults to tailor results to their organization's preferences. Secret Detection now supports disabling existing rules and adding new regex patterns that allow the detection of any type of custom secret.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/secret_detection/#custom-rulesets
            text: Documentation
            data_ga_name: Custom Rulesets for Secret Detection
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Full Git History Secret Detection
          categories:
            - Secret Detection
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Identify historical secrets that might be hiding in your older git commit history.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/secret_detection/#full-history-secret-scan
            text: Documentation
            data_ga_name: Full Git History Secret Detection
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Post Processing of Leaked Secrets
          categories:
            - Secret Detection
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Post-processing hooks for [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/). These can be used to take actions like notifying the cloud service that issued the secret. Post-processing workflows vary by supported cloud providers.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/secret_detection/#post-processing
            text: Documentation
            data_ga_name: Post Processing of Leaked Secrets
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Code Quality
        - title: Code Quality Reports
          categories:
            - Code Quality
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Full Code Quality reports are available on the pipeline page, showing areas of the codebase that do not meet the organization's preferred style or standards.
          link:
            href: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#code-quality-reports
            text: Documentation
            data_ga_name: Code Quality Reports
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Code Quality MR Widget
          categories:
            - Code Quality
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Code Quality reports are available in the merge request widget area, giving you early insights into how the change will affect the health of your code before deciding if you want to accept it.
          link:
            href: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
            text: Documentation
            data_ga_name: Code Quality MR Widget
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Code Quality violation notices in MR diffs
          categories:
            - Code Quality
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Code Quality violations introduced in a merge request are annotated in the merge request diff view to detail how the code quality could decrease if merged.
          link:
            href: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#code-quality-in-diff-view
            text: Documentation
            data_ga_name: Code Quality violation notices in MR diffs
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        # Dynamic Application Security Testing
        - title: Dynamic Application Security Testing
          categories:
            - Dynamic Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Ensure you are not exposed to web application vulnerabilities like broken authentication, cross-site scripting, or SQL injection by dynamically investigating your running test applications in CI/CD pipelines.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dast/
            text: Documentation
            data_ga_name: Dynamic Application Security Testing
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: On-demand DAST
          categories:
            - Dynamic Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Identify vulnerabilities in your running application, independent of code changes or merge requests.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dast/#on-demand-scans
            text: Documentation
            data_ga_name: On-demand DAST
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: DAST Configuration UI
          categories:
            - Dynamic Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Enabling DAST is now as simple as three clicks. This guided configuration experience makes it easier for non-CI experts to get started with GitLab DAST. The tool helps a user create a merge request to enable DAST scanning while leveraging best configuration practices like using the GitLab-managed [`DAST.gitlab-ci.yml` template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml).
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dast/#configure-dast-using-the-ui
            text: Documentation
            data_ga_name: DAST Configuration UI
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Site and Scanner profiles for On-demand DAST scans
          categories:
            - Dynamic Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Reuse configuration profiles quickly with on-demand DAST scans, instead of reconfiguring scans every time you need to run one. Mix different scan profiles with site profiles to quickly conduct scans that cover different areas or depths of your application and API.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dast/#site-profile
            text: Documentation
            data_ga_name: Site and Scanner profiles for On-demand DAST scans
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Scheduling On-demand DAST scans
          categories:
            - Dynamic Application Security Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Set on-demand DAST scans to run on ad hoc or recurring schedules.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dast/#schedule-an-on-demand-scan
            text: Documentation
            data_ga_name: Scheduling On-demand DAST scans
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        # API Security
        - title: Dynamic API Security Testing (DAST API)
          categories:
            - API Security
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Gain insight into vulnerabilities across your entire running application's attack surface, not just your UI. Leverages Postman collection, HAR files, and OpenAPI specifications to automatically discover and dynamically test URLs and API endpoints.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dast_api/
            text: Documentation
            data_ga_name: Dynamic API Security Testing (DAST API)
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: API Fuzz Testing
          categories:
            - API Security
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Test the APIs in your apps to find vulnerabilities and bugs that traditional QA processes miss.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/api_fuzzing/index.html
            text: Documentation
            data_ga_name: API Fuzz Testing
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        # Fuzz Testing
        - title: Coverage-guided Fuzz Testing
          categories:
            - Fuzz Testing
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Find security vulnerabilities and bugs in your app that traditional QA processes miss.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
            text: Documentation
            data_ga_name: Coverage-guided Fuzz Testing
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        # Dependency Scanning
        - title: Automated solutions for Dependency Scanning vulnerabilities
          categories:
            - Dependency Scanning
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Download and apply a patch to fix vulnerabilities affecting your codebase.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/index.html#solutions-for-vulnerabilities-auto-remediation
            text: Documentation
            data_ga_name: Automated solutions for Dependency Scanning vulnerabilities
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Project Dependency List
          categories:
            - Dependency Scanning
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Identify components included in your project by accessing the Dependency List (also referred to as Bill of Materials or BOM) ,which is often requested by Security and Compliance teams.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#dependency-list
            text: Documentation
            data_ga_name: Project Dependency List
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Dependency Scanning
          categories:
            - Dependency Scanning
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Protect your application from vulnerabilities that affect dynamic dependencies by automatically detecting well-known security bugs in your included libraries.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
            text: Documentation
            data_ga_name: Dependency Scanning
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        # License Compliance
        - title: License Compliance
          categories:
            - License Compliance
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Check that licenses of your dependencies are compatible with your application, and approve or deny them. Results are then shown in the Merge Request and in the Pipeline view.
          link:
            href: https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html
            text: Documentation
            data_ga_name: License Compliance
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        # Vulnerability Database
        - title: Vulnerability Database
          categories:
            - Vulnerability Database
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: A vulnerability database that can be viewed and enhanced by anyone.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#contributing-to-the-vulnerability-database
            text: Documentation
            data_ga_name: Vulnerability Database
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        # Vulnerability Management
        - title: Standalone Vulnerability Objects
          categories:
            - Vulnerability Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Track and manage detected project vulnerabilities like you would an Issue. Link directly to a specific vulnerability occurrence's page, create and link a remediation issue, and see vulnerability information persisted between security scans on the same branch.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/#interacting-with-the-vulnerabilities
            text: Documentation
            data_ga_name: Standalone Vulnerability Objects
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Vulnerability Reports
          categories:
            - Vulnerability Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Vulnerability Reports give teams an effient way to view, triage, track, and resolve vulnerabilities detected in applications, giving you full visibility into your organization’s risk. They are available for groups, projects, and the Security Center.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/#interacting-with-the-vulnerabilities
            text: Documentation
            data_ga_name: Vulnerability Reports
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Security Dashboards
          categories:
            - Vulnerability Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Gain visibility into top-priority fixes by identifying and tracking trends in security risk across your entire organization.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html
            text: Documentation
            data_ga_name: Security Dashboards
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Vulnerability Management
          categories:
            - Vulnerability Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Empower your entire team, and not just Security, to act on security findings with a unified interface for scan results from all GitLab Security scanners.
          link:
            href: /direction/secure/vulnerability_management/
            text: Learn more
            data_ga_name: Vulnerability Management
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Create Jira issues from vulnerabilities
          categories:
            - Vulnerability Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Efficiently collaborate between teams using GitLab for security testing and Jira for agile planning. Create a Jira issue type of your choosing directly from a vulnerability record.
          link:
            href: https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#create-a-gitlab-issue-for-a-vulnerability
            text: Documentation
            data_ga_name: Create Jira issues from vulnerabilities
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Security Scan summary in Merge Requests
          categories:
            - Vulnerability Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: All merge requests will show a helpful high level security scan summary of finding severities if there have been security scans run. This helps developers understand the risk of introduced vulnerabilities and helps users easily find secure job artifacts. Ultimate customers will continue to enjoy [Vulnerability Management features](http://docs.gitlab.com/ee/user/application_security/#security-scanning-tools) across [all our scan types](http://docs.gitlab.com/ee/user/application_security/#security-scanning-tools).
          link:
            href: /releases/2020/10/22/gitlab-13-5-released/#improved-merge-request-experience-for-security-scans
            text: Documentation
            data_ga_name: Security Scan summary in Merge Requests
            data_ga_location: body
          saas:
            - free
            - premium
          self_managed:
            - free
            - premium
        - title: Integrated security training
          categories:
            - Vulnerability Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Enable security training from our content partners to see lessons embedded in the vulnerability management experience. Links to training are dynamically provided in merge request security scan results, the pipeline security tab, and vulnerability details pages. We use the type of security issue and project language to provide the best available match for the most relevant, targeted learning experience.
          saas:
            - ultimate
          self_managed:
            - ultimate



